import java.sql.*;

public class tester {
    public static void main(String[] args) throws SQLException {

        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String dbURL = "jdbc:sqlserver://localhost;database=Bank;user=rpuser;password=1234";
        Connection conn = DriverManager.getConnection(dbURL);
        if (conn != null) {
            System.out.println("Connected");
        }
        PreparedStatement ps;
        String sql = "SELECT AccBalance FROM Accounts WHERE AccUsrName = user11";
        ps = MyConnection.getConnection().prepareStatement(sql);

        ResultSet result = ps.executeQuery();

        int count = 0;
        String fin_result = null;
        while (result.next()) {
            String usr_bal = result.getString(1);


            String output = "d: %s , %s";
            System.out.println(String.format(output, ++count, usr_bal));
            fin_result = usr_bal;
        }
        //statement end


    }
}
