import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
    public static Connection getConnection() throws SQLException {
        //driver initialzation
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
        }

        String dbURL = "jdbc:sqlserver://localhost;database=Bank;user=rpuser;password=1234";
        Connection conn = DriverManager.getConnection(dbURL);
        if (conn != null) {
            System.out.println("Connected");
        }
        //eof

            return conn;
    }
}

