import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class registrationForm2 extends JFrame {
    private JTextField textField1;
    private JPasswordField passwordField1;
    private JButton exitButton;
    private JButton registerButton;
    private JPanel register;
    private JPasswordField passwordField2;

    public registrationForm2() {


        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PreparedStatement ps = null;
                ResultSet rs;
                String uname = textField1.getText();
                String pass = passwordField1.getText();
                String re_pass = passwordField2.getText();
                String sql = "INSERT into Accounts (AccUsrName, AccPasswd,AccBalance) VALUES (?,?,0)";

                if (uname.equals("")) {
                    JOptionPane.showMessageDialog(null, "Add A Username");
                } else if (pass.equals("")) {
                    JOptionPane.showMessageDialog(null, "Add A Password");
                } else if (!pass.equals(re_pass)) {
                    JOptionPane.showMessageDialog(null, "Retype The Password Again");
                } else if (checkUsername(uname)) {
                    JOptionPane.showMessageDialog(null, "This Username Already Exist");
                } else {

                    try {
                        ps = MyConnection.getConnection().prepareStatement(sql);

                        ps.setString(1, uname);
                        ps.setString(2, pass);

                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    try {
                        assert ps != null;
                        if (ps.executeUpdate() > 0) {
                            JOptionPane.showMessageDialog(null, "New User Add");
                            registrationForm2.this.dispose();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    JComponent comp = (JComponent) e.getSource();
                    Window win = SwingUtilities.getWindowAncestor(comp);
                    win.dispose();

                    initiateLogin();


                }


            }

        });

        exitButton.addActionListener(e -> System.exit(0));
    }


    public void initiateLogin() {


        LoginForm2.main(null);


    }

    public JTextField getTextField1() {
        return textField1;
    }

    public boolean checkUsername(String uname) {
        PreparedStatement ps;
        ResultSet rs;
        boolean checkUser = false;
        String query = "SELECT * FROM Accounts WHERE AccUsrName =?";

        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, uname);

            rs = ps.executeQuery();

            if (rs.next()) {
                checkUser = true;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "username already taken!. Please choose a new Username");
        }
        return checkUser;
    }



    public static void main(String[] args) {
        String LAF = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

        try {
            UIManager.setLookAndFeel(LAF);
        } catch (UnsupportedLookAndFeelException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
            e.printStackTrace();
        }



        JFrame frame = new JFrame("registrationForm2");
        frame.setContentPane(new registrationForm2().register);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(420, 420);

        frame.setVisible(true);


    }
}
