import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Scanner;



public class Panel_Builder extends JFrame {

    private JLabel main;
    private JButton withdraw;
    private JButton deposit;
    private JButton check_balance;
    private JButton exitus;

    private JPanel main_menu;


    Scanner sc = new Scanner(System.in);



    public Panel_Builder() throws SQLException {
        Sql_Execute e1 = new Sql_Execute();

        exitus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        check_balance.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //displaying the total balance of the user
               // JOptionPane.showMessageDialog(null,"Balance : " + balance);


                try {
                    JOptionPane.showMessageDialog(null,"Balance: "+e1.balance_ispis());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }




            }
        });
        withdraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String amount = JOptionPane.showInputDialog("Enter money to be withdrawn:");
                //get the withdrawl money from user
                float amountf = Integer.parseInt(amount);
                try {
                    e1.withdraw_money((int) amountf);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                JOptionPane.showMessageDialog(null,"Money sucessfully withdrawn!");




            }
        });
        deposit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.print("Enter money to be deposited:");

                //get deposite amount from te user
                String amount = JOptionPane.showInputDialog("Enter money to be deposited:");
                float amountf = Float.parseFloat(amount);
                try {
                    e1.deposit_money((int) amountf);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                //add the deposit amount to the total balanace
                JOptionPane.showMessageDialog(null,"Your Money has been successfully deposited");


            }

        });
    }





    public static void main(String[] args) throws SQLException {
        String LAF="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

        java.awt.EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame("Panel_Builder");
            try {
                frame.setContentPane(new Panel_Builder().main_menu);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
            frame.pack();


        });

        try {
            UIManager.setLookAndFeel(LAF);
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        /*JFrame frame = new JFrame("Panel_Builder");
        frame.setContentPane(new Panel_Builder().main_menu);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.pack();*/

    }

}

