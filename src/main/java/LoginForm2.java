import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginForm2 extends JFrame{
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public void setLoginPanel(JPanel loginPanel) {
        this.loginPanel = loginPanel;
    }


     static String uname;
    private JPanel loginPanel;
    private JTextField UserField1;
    private JPasswordField passwordField1;
    private JButton logInButton;
    private JButton registerButton;
    public LoginForm2() {

        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PreparedStatement ps;
                ResultSet rs;
                String uname = UserField1.getText();
                setUname(uname);

                String pass = passwordField1.getText();


                String query = "SELECT * FROM Accounts WHERE AccUsrName =? AND AccPasswd =?";

                try {
                    ps = MyConnection.getConnection().prepareStatement(query);
                    System.out.println(uname);
                    ps.setString(1,uname);

                    ps.setString(2,pass);

                    rs = ps.executeQuery();

                    if (rs.next()) {
                        Panel_Builder.main(null);

                    } else {
                        JOptionPane.showMessageDialog(null, "Incorrect Username Or Password", "Login Failed", 2);

                    }


                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                JComponent comp = (JComponent) e.getSource();
                Window win = SwingUtilities.getWindowAncestor(comp);
                win.dispose();

            }

        });

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComponent comp = (JComponent) e.getSource();
                Window win = SwingUtilities.getWindowAncestor(comp);
                win.dispose();
                registrationForm2.main(null);




            }
        });
    }

    public static void main(String[] args) {
        String LAF="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";

        try {
            UIManager.setLookAndFeel(LAF);
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

            }
        });

        JFrame frame = new JFrame("LoginForm2");
        frame.setContentPane(new LoginForm2().loginPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setSize(420,420);
        frame.pack();




    }
}

