import javax.swing.*;
import java.sql.*;
import java.text.MessageFormat;

public class Sql_Execute{


    public  Sql_Execute() throws SQLException {
        connect();
    }







    public static void connect() throws SQLException {
        //driver initialzation

        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String dbURL = "jdbc:sqlserver://localhost;database=Bank;user=rpuser;password=1234";
        Connection conn = DriverManager.getConnection(dbURL);
        if (conn != null) {
            System.out.println("Connected");
        }

    }

    public Object balance_ispis() throws SQLException {


        //statement start
        PreparedStatement ps;
        System.out.println(LoginForm2.uname);
        String sql = "SELECT AccBalance FROM Accounts WHERE AccUsrName = ?";
        ps = MyConnection.getConnection().prepareStatement(sql);

        ps.setString(1,LoginForm2.uname);
        ResultSet result = ps.executeQuery();

        int count = 0;
        String fin_result = null;
        while (result.next()) {
            String usr_bal = result.getString(1);


            String output = "d: %s , %s";
            System.out.println(String.format(output, ++count, usr_bal));
            fin_result = usr_bal;
        }
        //statement end

        return fin_result;
    }

    public Object withdraw_money(int inpt) throws SQLException {


        PreparedStatement ps;

        //start of Statement
        Integer vari = inpt;
        /*String sql = String.format("DECLARE @var AS money = %d " +
                "UPDATE Accounts SET AccBalance = AccBalance-@var " +
                "where AccUsrName = 'test1' AND AccBalance >= %d " +
                "SELECT AccBalance from Accounts", vari, vari);*/
        String sql = "UPDATE Accounts set AccBalance = AccBalance-?  where AccUsrName = ?";
        ps = MyConnection.getConnection().prepareStatement(sql);
        ps.setString(1, String.valueOf(vari));
        ps.setString(2,LoginForm2.uname);
        ps.executeUpdate();
        return null;
    }

    public Object deposit_money(int inpt) throws SQLException {
        //driver initialzation



        PreparedStatement ps;
        //start of Statement

        Integer vari = inpt;
        /*String sql = String.format("DECLARE @var AS money = %d " +
                "UPDATE Accounts SET AccBalance = AccBalance+@var " +
                "where AccUsrName = 'test1' " +
                "SELECT AccBalance from Accounts", var);*/

        String sql = "UPDATE Accounts set AccBalance = AccBalance+ ?  where AccUsrName = ? ";

        ps = MyConnection.getConnection().prepareStatement(sql);
        ps.setString(1,String.valueOf(vari));
        ps.setString(2,LoginForm2.uname);

        ps.executeUpdate();

        return null;

    }
}
