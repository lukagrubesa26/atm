import java.sql.*;
import java.util.Scanner;

public class sql {
    public static void hello() throws SQLException {
        //driver initialzation
        try {
            DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        String dbURL = "jdbc:sqlserver://localhost;database=Bank;user=rpuser;password=1234";
        Connection conn = DriverManager.getConnection(dbURL);
        if (conn != null) {
            System.out.println("Connected");
        }

        //select

        String sql = "SELECT * FROM production.brands";
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);

        int count = 0;
        while (result.next()) {
            String brand_id = result.getString(1);
            String brand_name = result.getString(2);


            String output = "d: %s , %s";
            System.out.println(String.format(output, ++count, brand_id, brand_name));

        }


        //insert
        System.out.println("enter brand_name");
        Scanner scanner = new Scanner(System.in);
        String brand_id = scanner.nextLine();

        String sql2 = "INSERT INTO production.brands (brand_name) VALUES (?)";

        PreparedStatement statement2 = conn.prepareStatement(sql2);
        statement2.setString(2,brand_id);


        int rowsInserted = statement2.executeUpdate();
        if (rowsInserted > 0) {
            System.out.println("A new user was inserted successfully!");
        }


        //update
        System.out.println("id of the brand you want to update");

        String brand_id2 = scanner.nextLine();
        System.out.println("update the name of the brand");
        String brand_id3 = scanner.nextLine();
        String sql3= "UPDATE production.brands SET brand_name = ? Where brand_id = ?";
        PreparedStatement statement3 = conn.prepareStatement(sql3);
        statement3.setString(1,brand_id3 );
        statement3.setString(2, brand_id2);


        int rowsUpdated = statement3.executeUpdate();
        if (rowsUpdated > 0) {
            System.out.println("An existing user was updated successfully!");
        }
    }
}